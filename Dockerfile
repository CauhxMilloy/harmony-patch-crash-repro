
FROM alpine:3.3

RUN apk update --no-cache --update-cache

RUN apk add --no-cache \
    --upgrade --repository http://dl-cdn.alpinelinux.org/alpine/edge/main \
    apk-tools

RUN apk add --no-cache \
    --update-cache --repository http://dl-4.alpinelinux.org/alpine/edge/testing \
    mono

COPY ./Harmony.1.2.0.1/Release/net45/0Harmony.dll /root/Harmony1/0Harmony.dll
COPY ./Harmony.2.1.0.0/net45/0Harmony.dll /root/Harmony2/0Harmony.dll

COPY ./repro_case.cs /root/repro_case.cs
COPY ./build_and_run_repro_case.sh /root/build_and_run_repro_case.sh

ARG DOT_NET_VERSION_ARG="4.6.1"
ENV DOT_NET_VERSION="$DOT_NET_VERSION_ARG"

ARG USE_HARMONY_2_ARG=""
ENV USE_HARMONY_2="$USE_HARMONY_2_ARG"

ARG PATCH_AND_THROW_FOR_CONSTRUCTOR_ARG=""
ENV PATCH_AND_THROW_FOR_CONSTRUCTOR="$PATCH_AND_THROW_FOR_CONSTRUCTOR_ARG"

ARG SUPERFICIALLY_FIX_ISSUE_ARG=""
ENV SUPERFICIALLY_FIX_ISSUE="$SUPERFICIALLY_FIX_ISSUE_ARG"

ARG SUPPRESS_PATCHED_CODE_ARG=""
ENV SUPPRESS_PATCHED_CODE="$SUPPRESS_PATCHED_CODE_ARG"

RUN if [ ! -z "$USE_HARMONY_2" ]; then cp /root/Harmony2/0Harmony.dll /root/0Harmony.dll ; else cp /root/Harmony1/0Harmony.dll /root/0Harmony.dll ; fi

WORKDIR /root

CMD ["/root/build_and_run_repro_case.sh"]
