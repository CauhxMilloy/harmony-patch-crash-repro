#! /bin/bash

if [ ! -f './Harmony.2.1.0.0.zip' ]; then
  curl -sL 'https://github.com/pardeike/Harmony/releases/download/v2.1.0.0/Harmony.2.1.0.0.zip' --output 'Harmony.2.1.0.0.zip'
fi
if [ ! -d 'Harmony.2.1.0.0' ]; then
  unzip 'Harmony.2.1.0.0.zip' -d 'Harmony.2.1.0.0'
fi

if [ ! -f './Harmony.1.2.0.1.zip' ]; then
  curl -sL 'https://github.com/pardeike/Harmony/releases/download/v1.2.0.1/Release.zip' --output 'Harmony.1.2.0.1.zip'
fi
if [ ! -d 'Harmony.1.2.0.1' ]; then
  unzip 'Harmony.1.2.0.1.zip' -d 'Harmony.1.2.0.1'
fi


while (( "$#" )); do
    case "$1" in
        --h2)
        USE_HARMONY_2='true'
        shift
        ;;
        --ctor)
        PATCH_AND_THROW_FOR_CONSTRUCTOR='true'
        shift
        ;;
        --fix)
        SUPERFICIALLY_FIX_ISSUE='true'
        shift
        ;;
        --suppress)
        SUPPRESS_PATCHED_CODE='true'
        shift
        ;;
        --dot-net-version=*)
        DOT_NET_VERSION="${1#*=}"
        shift
        ;;
        --dot-net-version)
        DOT_NET_VERSION="$2"
        shift 2
        ;;
        *)
        echo "Error: Unsupported flag $1" >&2
        exit 1
        ;;
    esac
done

docker build \
    ${DOT_NET_VERSION:+--build-arg DOT_NET_VERSION_ARG=$DOT_NET_VERSION} \
    ${USE_HARMONY_2:+--build-arg USE_HARMONY_2_ARG=true} \
    ${PATCH_AND_THROW_FOR_CONSTRUCTOR:+--build-arg PATCH_AND_THROW_FOR_CONSTRUCTOR_ARG=true} \
    ${SUPERFICIALLY_FIX_ISSUE:+--build-arg SUPERFICIALLY_FIX_ISSUE_ARG=true} \
    ${SUPPRESS_PATCHED_CODE:+--build-arg SUPPRESS_PATCHED_CODE_ARG=true} \
    -t repro_case_image . \
&& docker run --rm -it repro_case_image
