#! /bin/sh

EXTRA_DEFINES="${USE_HARMONY_2:+;USE_HARMONY_2}${SUPERFICIALLY_FIX_ISSUE:+;SUPERFICIALLY_FIX_ISSUE}${PATCH_AND_THROW_FOR_CONSTRUCTOR:+;PATCH_AND_THROW_FOR_CONSTRUCTOR}${SUPPRESS_PATCHED_CODE:+;SUPPRESS_PATCHED_CODE}"

echo "EXTRA_DEFINES: $EXTRA_DEFINES"
echo "DOT_NET_VERSION: $DOT_NET_VERSION"

csc \
    repro_case.cs \
    /out:repro_case.exe \
    /target:exe \
    /define:"DEBUG;TRACE$EXTRA_DEFINES" \
    /reference:0Harmony.dll \
    /nostdlib \
    /reference:/usr/lib/mono/${DOT_NET_VERSION}-api/mscorlib.dll

mono --debug --optimize=-inline ./repro_case.exe
