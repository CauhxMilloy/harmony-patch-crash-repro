
using System;
using System.Reflection;


#if USE_HARMONY_2
using HarmonyLib;
#else
using Harmony;
#endif


public class CReproCaseLib
{
    public static void Main(string[] args)
    {
        Console.WriteLine("Running repro case main..");

        CReproCaseLib.Run();

        Console.WriteLine("Finished.");
    }

    public static void Run()
    {
#if USE_HARMONY_2
        Console.WriteLine("Using Harmony 2");
#else
        Console.WriteLine("Using Harmony 1");
#endif

        PrintGeneralDescriptionOfExpectations();

        MethodInfo oPatchPrefixMethod = typeof(CReproCaseLib).GetMethod("PatchPrefix0");

#if PATCH_AND_THROW_FOR_CONSTRUCTOR
        MethodBase oMethodToPatch = typeof(CClassToBePatched).GetConstructor(Type.EmptyTypes);
#else
        MethodBase oMethodToPatch = typeof(CClassToBePatched).GetMethod("SomeFunc0");
#endif

#if USE_HARMONY_2
        Harmony oHarmonyInstance = new Harmony(HARMONY_INSTANCE_ID);
        Patches oPatchesOnMethod = Harmony.GetPatchInfo(oMethodToPatch);
#else
        HarmonyInstance oHarmonyInstance = HarmonyInstance.Create(HARMONY_INSTANCE_ID);
        Patches oPatchesOnMethod = oHarmonyInstance.GetPatchInfo(oMethodToPatch);
#endif

        if ((oPatchesOnMethod == null) || !oPatchesOnMethod.Owners.Contains(HARMONY_INSTANCE_ID))
        {
            oHarmonyInstance.Patch(
                oMethodToPatch,
                new HarmonyMethod(oPatchPrefixMethod));
        }

        Console.WriteLine("Patch complete.");

        try
        {
            CClassToBePatched oInstance = new CClassToBePatched();
            oInstance.SomeFunc0();
        }
        catch (Exception oExcep)
        {
            Console.WriteLine("Expected exception: " + oExcep.Message);
        }
    }

    public class CClassToBePatched
    {
        public CClassToBePatched()
        {
            Console.WriteLine("In CClassToBePatched ctor.");
#if PATCH_AND_THROW_FOR_CONSTRUCTOR
#if SUPERFICIALLY_FIX_ISSUE
            if (iSomeVar == 0)
#endif
            {
                throw new Exception("threw in ctor");
            }
#endif
        }

        public void SomeFunc0()
        {
#if SUPERFICIALLY_FIX_ISSUE
            Console.WriteLine("In SomeFunc0.");
#endif
        }

#pragma warning disable 0169
        int iSomeVar;
#pragma warning restore 0169
    }

    public static bool PatchPrefix0(
        MethodInfo __originalMethod,
        CClassToBePatched __instance)
    {
        Console.WriteLine("In PatchPrefix0.");
#if SUPPRESS_PATCHED_CODE
        return false;
#else
        return true;
#endif
    }

    static void PrintGeneralDescriptionOfExpectations()
    {
        Console.WriteLine("====================");
#if PATCH_AND_THROW_FOR_CONSTRUCTOR

        Console.WriteLine("This run will be patching CClassToBePatched's constructor.");
#if SUPERFICIALLY_FIX_ISSUE
        Console.WriteLine("This will constructor conditionally (within branching if) throw an exception.");
#else
        Console.WriteLine("This will constructor unconditionally (always) throw an exception.");
#endif

#else

        Console.WriteLine("This run will be patching CClassToBePatched's SomeFunc0 method.");
#if !SUPERFICIALLY_FIX_ISSUE
        Console.WriteLine("The SomeFunc0 method is empty (no instructions in method body).");
#else
        Console.WriteLine("The SomeFunc0 method is nonempty (has at least 1 instruction in method body).");
#endif

#endif

#if SUPPRESS_PATCHED_CODE
        Console.WriteLine("The patched functionality will not run the original after the prefix.");
#else
        Console.WriteLine("The patched functionality will run the original after the prefix.");
#endif

#if !USE_HARMONY_2 && !PATCH_AND_THROW_FOR_CONSTRUCTOR && !SUPERFICIALLY_FIX_ISSUE
        Console.WriteLine("The expected (no bug) functionality would be:");
        Console.WriteLine("\t\"Patch complete.\" is printed.");
        Console.WriteLine("\t\"In CClassToBePatched ctor.\" is printed.");
        Console.WriteLine("\t\"In PatchPrefix0.\" is printed.");
        Console.WriteLine("\t\"Finished.\" is printed.");

        Console.WriteLine("The actual (bugged) functionality is:");
        Console.WriteLine("\t\"Patch complete.\" is printed.");
        Console.WriteLine("\t\"In CClassToBePatched ctor.\" is printed.");
        Console.WriteLine("\t\"Crashes with varying reports (see README).");

#elif USE_HARMONY_2 && !PATCH_AND_THROW_FOR_CONSTRUCTOR && !SUPERFICIALLY_FIX_ISSUE
        Console.WriteLine("The expected (no bug) functionality would be:");
        Console.WriteLine("\t\"Patch complete.\" is printed.");
        Console.WriteLine("\t\"In CClassToBePatched ctor.\" is printed.");
        Console.WriteLine("\t\"In PatchPrefix0.\" is printed.");
        Console.WriteLine("\t\"Finished.\" is printed.");

        Console.WriteLine("The actual (bugged) functionality is:");
        Console.WriteLine("\tActually correct... here.");
        Console.WriteLine("\tIt crashes in another project using Harmony 2 (seemingly same crash as with Harmony 1), but I could not reproduce here sadly.");
        Console.WriteLine("\tSee README and/or run with Harmony 1 to see the crash.");

#elif USE_HARMONY_2 && PATCH_AND_THROW_FOR_CONSTRUCTOR && !SUPERFICIALLY_FIX_ISSUE
        Console.WriteLine("The expected (no bug) functionality would be:");
        Console.WriteLine("\t\"Patch complete.\" is printed.");
        Console.WriteLine("\t\"In PatchPrefix0.\" is printed.");
        Console.WriteLine("\t\"In CClassToBePatched ctor.\" is printed.");
        Console.WriteLine("\t\"Expected exception: threw in ctor\" is printed.");
        Console.WriteLine("\t\"Finished.\" is printed.");

        Console.WriteLine("The actual (bugged) functionality is:");
        Console.WriteLine("\t\"Crashes due to `System.Reflection.TargetInvocationException` being thrown from within `System.Reflection.Emit.ILGenerator.label_fixup`.");
#elif SUPERFICIALLY_FIX_ISSUE && (!PATCH_AND_THROW_FOR_CONSTRUCTOR || USE_HARMONY_2)

        Console.WriteLine("The bugs are \"superficially fixed\". No crash will occur.");
#else

        Console.WriteLine("There's no associated bug with this run configuration.");
#endif
        Console.WriteLine("====================");
    }

    const String HARMONY_INSTANCE_ID = "simple.repro.case.thing";
}