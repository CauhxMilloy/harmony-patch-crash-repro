
# Repro Cases for Harmony Bugs

These bugs are found with Mono 6.12.0.122 (current as of July 2021).

## Project Overview

This project expects to be run via docker.
Its main script, `build_and_run_docker.sh`, should be run from a linux distro (tested using Ubuntu) which has Docker installed.\
Technically, this project can be run from any OS that has Docker. It just may or may not be able to run the script.

Simply run `build_and_run_docker.sh` to build the docker image and run it. The built image is from `alpine:3.3` and installs `mono`. The container ran will build the given .cs files and runs them, exposing various bugs.

*Note:* When building the docker image for the first time, installing `mono` may take a while (a few minutes).

## Script Args

* Pass `--h2` to use Harmony 2.1.0.0 instead of the (default) 1.2.0.1.
* Pass `--fix` to change the code to superficially (impractically) "fix" the issue.
* Pass `--ctor` to patch a constructor instead of a method (different bug, see [below](#patching-constructor-that-throws-exception)).
* Pass `--suppress` to block the patched constructor/method from running (this does not affect the bugs, included to make that clear).
* Pass `--dot-net-version=<version>` or `--dot-net-version <version>` to change which versions of .NET are used to compile with. Given `find /usr/lib/mono/ -name *mscorlib.dll`, valid versions are:
  * `2.0`
  * `4.0`
  * `4.5`
  * `4.5.1`
  * `4.5.2`
  * `4.6`
  * `4.6.1`
  * `4.6.2`
  * `4.7`
  * `4.7.1`
  * `4.7.2`
  * `4.8`

I've tried all listed versions. They all fail. Though once (out of many many times), it didn't crash when using 4.5. :thinking:

## Bugs Demonstrated

There are 2 main bugs shown here:
  * [Patching Empty Methods](#patching-empty-methods)
    * See crash on Harmony 1 & 2 (in other project).
    * Repro case (this project) only shows crash on Harmony 1 (unsure why I couldn't get 2 to crash also).
    * Crashes for `mono` 6.12.0.122, fine for 5.20.1.19 (not readily available anymore).
    * Shown in project by running `build_and_run_docker.sh`.
  * [Patching Constructor That Throws Exception](#patching-constructor-that-throws-exception)
    * Only happens on Harmony 2.
    * Shown in project by running `build_and_run_docker.sh --h2 --ctor`.

### Patching Empty Methods

When patching an empty method (empty method body), the program will crash. This is shown via either a `SIGSEGV`, `SIGABRT`, `UNKNOWN`, or `System.NullReferenceException`. If the method has any instructions in it, no issue is seen when patching.

I have seen this issue happen in both Harmony 1 (1.2.0.1) and Harmony 2 (2.1.0.0). However, I could only get this repro case to show it with Harmony 1. Harmony 2 (in this project) seems to work fine for this bug... but I've seen it (in larger/more complex code bases) fail when trying Harmony 2 as well. I understand that Harmony 1 is not under dev (now that there's Harmony 2); but as I've seen this happen with Harmony 2 elsewhere, I thought that it could be the same underlying issue causing it.

It is worth noting that they did not exist in an older version of mono, 5.20.1.19 (2019). I happened to find this bug when updating a 2-year-old docker image. :sweat_smile:

<details>
  <summary>Simplified Example</summary>

```
    public static void Run()
    {
        MethodBase oMethodToPatch = typeof(CClassToBePatched).GetMethod("SomeFunc0");
        MethodInfo oPatchPrefixMethod = typeof(CReproCaseLib).GetMethod("PatchPrefix0");

        // For Harmony 1
        HarmonyInstance oHarmonyInstance = HarmonyInstance.Create(HARMONY_INSTANCE_ID);
        // For Harmony 2
        Harmony oHarmonyInstance = new Harmony(HARMONY_INSTANCE_ID);
        oHarmonyInstance.Patch(
            oMethodToPatch,
            new HarmonyMethod(oPatchPrefixMethod));

        CClassToBePatched oInstance = new CClassToBePatched();
        oInstance.SomeFunc0();
    }

    public class CClassToBePatched
    {
        public void SomeFunc0()
        {
        }
    }

    public static bool PatchPrefix0(
        MethodInfo __originalMethod,
        CClassToBePatched __instance)
    {
        return false;
    }
```
</details>

#### Examples crash reports:

The output of a given crash is often not consistent.

<details>
  <summary>Example Crash Reports (in no particular order)</summary>

```
=================================================================
        Native Crash Reporting
=================================================================
Got a SIGSEGV while executing native code. This usually indicates
a fatal error in the mono runtime or one of the native libraries 
used by your application.
=================================================================

=================================================================
        Basic Fault Address Reporting
=================================================================
Memory around native instruction pointer (0x40ca7a62):0x40ca7a52  40 a1 d4 2f 7c 7f 00 00 ff e0 05 01 08 e8 f0 5b  @../|..........[
0x40ca7a62  30 7c 7f 00 00 e8 94 f7 05 01 08 e0 e4 da 2f 7c  0|............/|
0x40ca7a72  7f 00 00 00 00 00 00 00 00 00 00 00 00 00 48 83  ..............H.
0x40ca7a82  ec 38 4c 89 34 24 4c 89 7c 24 08 4c 8b f7 4c 8b  .8L.4$L.|$.L..L.

=================================================================
        Managed Stacktrace:
=================================================================
=================================================================
Aborted (core dumped)
```

```
=================================================================
        Native Crash Reporting
=================================================================
Got a UNKNOWN while executing native code. This usually indicates
a fatal error in the mono runtime or one of the native libraries 
used by your application.
=================================================================

=================================================================
        Basic Fault Address Reporting
=================================================================
Memory around native instruction pointer (0x4053eaa4):0x4053ea94  b8 48 f5 bd 0e 3c 56 00 00 f7 00 01 00 00 00 74  .H...<V........t
0x4053eaa4  07 66 90 e8 ae 70 fa ff 49 8b fe e8 2c 02 00 00  .f...p..I...,...
0x4053eab4  66 90 49 bb a0 fd 7f fa 72 7f 00 00 41 ff d3 83  f.I.....r...A...
0x4053eac4  f8 08 40 0f 94 c0 48 0f b6 c0 48 0f b6 c0 85 c0  ..@...H...H.....

=================================================================
        Managed Stacktrace:
=================================================================
          at Harmony.ILCopying.Memory:WriteJump <0x00024>
=================================================================
Aborted (core dumped)
```

```
=================================================================
        Native Crash Reporting
=================================================================
Got a UNKNOWN while executing native code. This usually indicates
a fatal error in the mono runtime or one of the native libraries 
used by your application.
=================================================================

=================================================================
        Basic Fault Address Reporting
=================================================================
Memory around native instruction pointer (0x412e99e4):0x412e99d4  8b ff 48 89 64 24 38 ff d0 48 8b c8 48 b8 80 98  ..H.d$8..H..H...
0x412e99e4  60 ad 13 56 00 00 8b 00 4c 8b f9 85 c0 0f 85 41  `..V....L......A
0x412e99f4  00 00 00 eb 00 48 8b 44 24 28 49 89 06 49 8b c7  .....H.D$(I..I..
0x412e9a04  48 8b 1c 24 4c 8b 64 24 08 4c 8b 6c 24 10 4c 8b  H..$L.d$.L.l$.L.

=================================================================
        Managed Stacktrace:
=================================================================
          at System.RuntimeMethodHandle:GetFunctionPointer <0x00074>
=================================================================
Aborted (core dumped)
```

```
0
* Assertion: should not be reached at tramp-amd64.c:234


=================================================================
        Native Crash Reporting
=================================================================
Got a SIGABRT while executing native code. This usually indicates
a fatal error in the mono runtime or one of the native libraries 
used by your application.
=================================================================

=================================================================
        Basic Fault Address Reporting
=================================================================
Memory around native instruction pointer (0x7fe50e2773f2):0x7fe50e2773e2  00 00 b8 0e 00 00 00 31 d2 bf 02 00 00 00 0f 05  .......1........
0x7fe50e2773f2  c3 48 63 ff 50 b8 24 00 00 00 0f 05 48 89 c7 e8  .Hc.P.$.....H...
0x7fe50e277402  07 89 fd ff 5a c3 48 63 ff 50 48 63 f6 b8 3e 00  ....Z.Hc.PHc..>.
0x7fe50e277412  00 00 0f 05 48 89 c7 e8 ef 88 fd ff 5a c3 85 ff  ....H.......Z...

=================================================================
        Managed Stacktrace:
=================================================================
=================================================================
Aborted (core dumped)
```

```
4
* Assertion: should not be reached at tramp-amd64.c:234


=================================================================
        Native Crash Reporting
=================================================================
Got a SIGSEGV while executing native code. This usually indicates
a fatal error in the mono runtime or one of the native libraries 
used by your application.
=================================================================

=================================================================
        Basic Fault Address Reporting
=================================================================
Memory around native instruction pointer (0x1):0xfffffffffffffff1  Segmentation fault (core dumped)
```

```
=================================================================
        Native Crash Reporting
=================================================================
Got a SIGABRT while executing native code. This usually indicates
a fatal error in the mono runtime or one of the native libraries 
used by your application.
=================================================================

=================================================================
        Basic Fault Address Reporting
=================================================================
Memory around native instruction pointer (0x7f38a28703f2):0x7f38a28703e2  00 00 b8 0e 00 00 00 31 d2 bf 02 00 00 00 0f 05  .......1........
0x7f38a28703f2  c3 48 63 ff 50 b8 24 00 00 00 0f 05 48 89 c7 e8  .Hc.P.$.....H...
0x7f38a2870402  07 89 fd ff 5a c3 48 63 ff 50 48 63 f6 b8 3e 00  ....Z.Hc.PHc..>.
0x7f38a2870412  00 00 0f 05 48 89 c7 e8 ef 88 fd ff 5a c3 85 ff  ....H.......Z...

=================================================================
        Managed Stacktrace:
=================================================================
=================================================================
Aborted (core dumped)
```

```
=================================================================
        Native Crash Reporting
=================================================================
Got a SIGSEGV while executing native code. This usually indicates
a fatal error in the mono runtime or one of the native libraries 
used by your application.
=================================================================

=================================================================
        Basic Fault Address Reporting
=================================================================
Memory around native instruction pointer (0x41a82ad1):0x41a82ac1  ff d3 83 f8 08 40 0f 94 c0 48 0f b6 c0 48 0f b6  .....@...H...H..
0x41a82ad1  c0 85 c0 0f 84 12 01 00 00 be 01 00 00 00 48 bf  ..............H.
0x41a82ae1  f0 d3 04 8c de 7f 00 00 48 8d 64 24 00 49 bb 70  ........H.d$.I.p
0x41a82af1  a5 a2 41 00 00 00 00 41 ff d3 48 8b f0 83 78 18  ..A....A..H...x.

=================================================================
        Managed Stacktrace:
=================================================================
          at Harmony.ILCopying.Memory:WriteJump <0x00051>
=================================================================
```

```
=================================================================
        Native Crash Reporting
=================================================================
Got a SIGSEGV while executing native code. This usually indicates
a fatal error in the mono runtime or one of the native libraries 
used by your application.
=================================================================

=================================================================
        Basic Fault Address Reporting
=================================================================
instruction pointer is NULL, skip dumping
=================================================================
        Managed Stacktrace:
=================================================================
=================================================================
Aborted (core dumped)
```

```
Unhandled Exception:
System.NullReferenceException: Object reference not set to an instance of an object
  at Harmony.ILCopying.Memory.WriteJump (System.Int64 memory, System.Int64 destination) <0x41138a80 + 0x00002> in <ef472ad41f4948fdb52674edfe069979>:0 
  at (wrapper dynamic-method) CReproCaseLib+CClassToBePatched.SomeFunc0_Patch1(object)
  at CReproCaseLib.Run () [0x00082] in /root/repro_case_lib.cs:48 
  at CReproCaseExe.Main (System.String[] args) [0x0000c] in /root/repro_case_exe.cs:12 
[ERROR] FATAL UNHANDLED EXCEPTION: System.NullReferenceException: Object reference not set to an instance of an object
  at Harmony.ILCopying.Memory.WriteJump (System.Int64 memory, System.Int64 destination) <0x41138a80 + 0x00002> in <ef472ad41f4948fdb52674edfe069979>:0 
  at (wrapper dynamic-method) CReproCaseLib+CClassToBePatched.SomeFunc0_Patch1(object)
  at CReproCaseLib.Run () [0x00082] in /root/repro_case_lib.cs:48 
  at CReproCaseExe.Main (System.String[] args) [0x0000c] in /root/repro_case_exe.cs:12
```

```
Unhandled Exception:
System.NullReferenceException: Object reference not set to an instance of an object
  at (wrapper managed-to-native) System.RuntimeMethodHandle.GetFunctionPointer(intptr)
  at CReproCaseExe.Main (System.String[] args) [0x00001] in /root/repro_case_exe.cs:10 
[ERROR] FATAL UNHANDLED EXCEPTION: System.NullReferenceException: Object reference not set to an instance of an object
  at (wrapper managed-to-native) System.RuntimeMethodHandle.GetFunctionPointer(intptr)
  at CReproCaseExe.Main (System.String[] args) [0x00001] in /root/repro_case_exe.cs:10 
```

```
Unhandled Exception:
System.NullReferenceException: Object reference not set to an instance of an object
  at (wrapper managed-to-native) System.RuntimeMethodHandle.GetFunctionPointer(intptr)
[ERROR] FATAL UNHANDLED EXCEPTION: System.NullReferenceException: Object reference not set to an instance of an object
  at (wrapper managed-to-native) System.RuntimeMethodHandle.GetFunctionPointer(intptr)
```

```
Segmentation fault (core dumped)
```
</details>

### Patching Constructor That Throws Exception

When patching a constructor that contains an unbranched `throw`, the patch attempt will throw an exception.

This only happens on Harmony 2 (2.1.0.0), and happens consistently with the same error every time.

<details>
  <summary>Example Crash Report</summary>

```
Unhandled Exception:
System.Reflection.TargetInvocationException: Exception has been thrown by the target of an invocation. ---> System.ArgumentException: Label #2 is not marked in method `CReproCaseLib+CClassToBePatched..ctor_Patch1'
  at System.Reflection.Emit.ILGenerator.label_fixup (System.Reflection.MethodBase mb) [0x0010c] in /home/buildozer/aports/testing/mono/src/mono-6.12.0.122/mcs/class/corlib/System.Reflection.Emit/ILGenerator.cs:1034 
  at System.Reflection.Emit.DynamicMethod.CreateDynMethod () [0x00056] in /home/buildozer/aports/testing/mono/src/mono-6.12.0.122/mcs/class/corlib/System.Reflection.Emit/DynamicMethod.cs:143 
  at (wrapper managed-to-native) System.Reflection.RuntimeMethodInfo.InternalInvoke(System.Reflection.RuntimeMethodInfo,object,object[],System.Exception&)
  at System.Reflection.RuntimeMethodInfo.Invoke (System.Object obj, System.Reflection.BindingFlags invokeAttr, System.Reflection.Binder binder, System.Object[] parameters, System.Globalization.CultureInfo culture) [0x0006a] in /home/buildozer/aports/testing/mono/src/mono-6.12.0.122/mcs/class/corlib/System.Reflection/RuntimeMethodInfo.cs:395 
   --- End of inner exception stack trace ---
  at System.Reflection.RuntimeMethodInfo.Invoke (System.Object obj, System.Reflection.BindingFlags invokeAttr, System.Reflection.Binder binder, System.Object[] parameters, System.Globalization.CultureInfo culture) [0x0007e] in /home/buildozer/aports/testing/mono/src/mono-6.12.0.122/mcs/class/corlib/System.Reflection/RuntimeMethodInfo.cs:409 
  at System.Reflection.MethodBase.Invoke (System.Object obj, System.Object[] parameters) [0x00000] in /home/buildozer/aports/testing/mono/src/mono-6.12.0.122/external/corefx/src/Common/src/CoreLib/System/Reflection/MethodBase.cs:53 
  at MonoMod.RuntimeDetour.Platforms.DetourRuntimeMonoPlatform.GetMethodHandle (System.Reflection.MethodBase method) [0x00011] in <9dbc449b67ac4724ad23ab74dde0cd28>:0 
  at MonoMod.RuntimeDetour.Platforms.DetourRuntimeILPlatform+<>c__DisplayClass24_0.<Pin>b__0 (System.Reflection.MethodBase m) [0x00012] in <9dbc449b67ac4724ad23ab74dde0cd28>:0 
  at System.Collections.Concurrent.ConcurrentDictionary`2[TKey,TValue].GetOrAdd (TKey key, System.Func`2[T,TResult] valueFactory) [0x00034] in <b4947f9d8e3644dfaa46480ad2f64100>:0 
  at MonoMod.RuntimeDetour.Platforms.DetourRuntimeILPlatform.Pin (System.Reflection.MethodBase method) [0x00014] in <9dbc449b67ac4724ad23ab74dde0cd28>:0 
  at MonoMod.RuntimeDetour.DetourHelper.Pin[T] (T method) [0x00005] in <9dbc449b67ac4724ad23ab74dde0cd28>:0 
  at HarmonyLib.MethodPatcher.CreateReplacement (System.Collections.Generic.Dictionary`2[System.Int32,HarmonyLib.CodeInstruction]& finalInstructions) [0x005a6] in <9dbc449b67ac4724ad23ab74dde0cd28>:0 
  at HarmonyLib.PatchFunctions.UpdateWrapper (System.Reflection.MethodBase original, HarmonyLib.PatchInfo patchInfo) [0x00057] in <9dbc449b67ac4724ad23ab74dde0cd28>:0 
  at HarmonyLib.PatchProcessor.Patch () [0x000fc] in <9dbc449b67ac4724ad23ab74dde0cd28>:0 
  at HarmonyLib.Harmony.Patch (System.Reflection.MethodBase original, HarmonyLib.HarmonyMethod prefix, HarmonyLib.HarmonyMethod postfix, HarmonyLib.HarmonyMethod transpiler, HarmonyLib.HarmonyMethod finalizer) [0x00028] in <9dbc449b67ac4724ad23ab74dde0cd28>:0 
  at CReproCaseLib.Run () [0x00070] in <eaeace7a30554cf8a406e14f28d5c5d3>:0 
  at CReproCaseExe.Main (System.String[] args) [0x0000c] in <c7be89f771a24ae5b5d5ebd475840720>:0 
[ERROR] FATAL UNHANDLED EXCEPTION: System.Reflection.TargetInvocationException: Exception has been thrown by the target of an invocation. ---> System.ArgumentException: Label #2 is not marked in method `CReproCaseLib+CClassToBePatched..ctor_Patch1'
  at System.Reflection.Emit.ILGenerator.label_fixup (System.Reflection.MethodBase mb) [0x0010c] in /home/buildozer/aports/testing/mono/src/mono-6.12.0.122/mcs/class/corlib/System.Reflection.Emit/ILGenerator.cs:1034 
  at System.Reflection.Emit.DynamicMethod.CreateDynMethod () [0x00056] in /home/buildozer/aports/testing/mono/src/mono-6.12.0.122/mcs/class/corlib/System.Reflection.Emit/DynamicMethod.cs:143 
  at (wrapper managed-to-native) System.Reflection.RuntimeMethodInfo.InternalInvoke(System.Reflection.RuntimeMethodInfo,object,object[],System.Exception&)
  at System.Reflection.RuntimeMethodInfo.Invoke (System.Object obj, System.Reflection.BindingFlags invokeAttr, System.Reflection.Binder binder, System.Object[] parameters, System.Globalization.CultureInfo culture) [0x0006a] in /home/buildozer/aports/testing/mono/src/mono-6.12.0.122/mcs/class/corlib/System.Reflection/RuntimeMethodInfo.cs:395 
   --- End of inner exception stack trace ---
  at System.Reflection.RuntimeMethodInfo.Invoke (System.Object obj, System.Reflection.BindingFlags invokeAttr, System.Reflection.Binder binder, System.Object[] parameters, System.Globalization.CultureInfo culture) [0x0007e] in /home/buildozer/aports/testing/mono/src/mono-6.12.0.122/mcs/class/corlib/System.Reflection/RuntimeMethodInfo.cs:409 
  at System.Reflection.MethodBase.Invoke (System.Object obj, System.Object[] parameters) [0x00000] in /home/buildozer/aports/testing/mono/src/mono-6.12.0.122/external/corefx/src/Common/src/CoreLib/System/Reflection/MethodBase.cs:53 
  at MonoMod.RuntimeDetour.Platforms.DetourRuntimeMonoPlatform.GetMethodHandle (System.Reflection.MethodBase method) [0x00011] in <9dbc449b67ac4724ad23ab74dde0cd28>:0 
  at MonoMod.RuntimeDetour.Platforms.DetourRuntimeILPlatform+<>c__DisplayClass24_0.<Pin>b__0 (System.Reflection.MethodBase m) [0x00012] in <9dbc449b67ac4724ad23ab74dde0cd28>:0 
  at System.Collections.Concurrent.ConcurrentDictionary`2[TKey,TValue].GetOrAdd (TKey key, System.Func`2[T,TResult] valueFactory) [0x00034] in <b4947f9d8e3644dfaa46480ad2f64100>:0 
  at MonoMod.RuntimeDetour.Platforms.DetourRuntimeILPlatform.Pin (System.Reflection.MethodBase method) [0x00014] in <9dbc449b67ac4724ad23ab74dde0cd28>:0 
  at MonoMod.RuntimeDetour.DetourHelper.Pin[T] (T method) [0x00005] in <9dbc449b67ac4724ad23ab74dde0cd28>:0 
  at HarmonyLib.MethodPatcher.CreateReplacement (System.Collections.Generic.Dictionary`2[System.Int32,HarmonyLib.CodeInstruction]& finalInstructions) [0x005a6] in <9dbc449b67ac4724ad23ab74dde0cd28>:0 
  at HarmonyLib.PatchFunctions.UpdateWrapper (System.Reflection.MethodBase original, HarmonyLib.PatchInfo patchInfo) [0x00057] in <9dbc449b67ac4724ad23ab74dde0cd28>:0 
  at HarmonyLib.PatchProcessor.Patch () [0x000fc] in <9dbc449b67ac4724ad23ab74dde0cd28>:0 
  at HarmonyLib.Harmony.Patch (System.Reflection.MethodBase original, HarmonyLib.HarmonyMethod prefix, HarmonyLib.HarmonyMethod postfix, HarmonyLib.HarmonyMethod transpiler, HarmonyLib.HarmonyMethod finalizer) [0x00028] in <9dbc449b67ac4724ad23ab74dde0cd28>:0 
  at CReproCaseLib.Run () [0x00070] in <eaeace7a30554cf8a406e14f28d5c5d3>:0 
  at CReproCaseExe.Main (System.String[] args) [0x0000c] in <c7be89f771a24ae5b5d5ebd475840720>:0 
```
</details>
